
(defreader dbg
  (setv parsed None)
  (setv str (.getc &reader))
  ; read one char at a time until it parses
  (while (not parsed)
    (try
      (setv parsed (hy.read str))
      (except [Exception]
        (setv str (+ str (.getc &reader))))))
  ; return a quoted expression
  `(let [result ~parsed]
    (print f"{~str} = {result}")
    result))


(when (= __name__ "__main__")
  #dbg(+ 2 2))
  ; prints "(+ 2 2) = 4"
